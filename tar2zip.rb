require 'sinatra'
require 'tmpdir'

class Tar2Zip < Sinatra::Base

  get '/' do
  
  <<END
  <!DOCTYPE html>
  <html>
  <body>
<h2>tar2zip FREE ONLINE SERVICE!</h2>

Just use the form below to convert *ANY* tar file to a ZIP file!
<br/>
<br/>
  <form action="/" method="post" enctype="multipart/form-data">
      Select TAR file to upload:
      <input type="file" name="file" id="file">
      <input type="submit" value="Upload tar file" name="submit">
  </form>
  
  </body>
  </html>
END
  
  end
  
  post '/' do
    tempfile = params['file'][:tempfile].path
    output = ""
    dir = Dir.mktmpdir { |dir|
      system("tar","-xf",tempfile,"--directory=#{dir}")
      system("zip","-r","#{dir}/output.zip",".",chdir: dir)
      output = File.read "#{dir}/output.zip"
    }
    attachment("converted.zip")
    output
  end
end

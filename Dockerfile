FROM ruby:2.6-alpine

RUN apk add zip
ENV APP_HOME /app
RUN mkdir $APP_HOME
WORKDIR $APP_HOME

ADD Gemfile* $APP_HOME/
RUN gem install bundler
RUN bundle install

ADD . $APP_HOME

EXPOSE 4567

CMD ["/app/script.sh"]
